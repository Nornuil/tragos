package com.example.tragosapp.ui

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import com.bumptech.glide.Glide
import com.example.tragosapp.AppDatabase
import com.example.tragosapp.R
import com.example.tragosapp.data.DataSourceImpl
import com.example.tragosapp.data.model.Drink
import com.example.tragosapp.data.model.DrinkEntity
import com.example.tragosapp.domain.RepoImpl
import com.example.tragosapp.ui.viewModel.MainViewModel
import com.example.tragosapp.ui.viewModel.VMFactory
import kotlinx.android.synthetic.main.fragment_tragos_detalle.*


class TragosDetalleFragment : Fragment() {

    private val viewModel by activityViewModels<MainViewModel> { VMFactory(
        RepoImpl(
            DataSourceImpl(AppDatabase.getDatabase(requireActivity().applicationContext))))
    }
    private lateinit var drink: Drink

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireArguments().let {
            //drink = it.getParcelable("drink")!!//buscar let run para colocar aca
            it.getParcelable<Drink>("drink")?.let{drink->
            this.drink = drink
            Log.d("DETALLES_FRAG","$drink")
            }?:run {
                Toast.makeText(context, "error", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tragos_detalle, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Glide.with(requireContext()).load(drink.imagen).centerCrop().into(imgTragoDetalle)
        tragoDescripcion.text = drink.descripcion
        if(drink.hasAlcohol == "Non_Alcoholic"){
            txtHasAlcohol.text = "Bebida sin alcohol"
        } else{
        txtHasAlcohol.text = "Bebida con alcohol"
        }

        btnGuardarTrago.setOnClickListener {
            viewModel.guardarTrago(DrinkEntity(drink.tragoId,drink.imagen,drink.nombre,drink.descripcion,drink.hasAlcohol))
            Toast.makeText(requireContext(), "Se guardó el trago en favoritos.", Toast.LENGTH_SHORT).show()
        }
    }
}