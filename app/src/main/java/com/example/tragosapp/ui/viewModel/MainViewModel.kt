package com.example.tragosapp.ui.viewModel


import androidx.lifecycle.*
import com.example.tragosapp.ValueObject.Resource
import com.example.tragosapp.data.model.Drink
import com.example.tragosapp.data.model.DrinkEntity
import com.example.tragosapp.domain.Repo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel(private val repo: Repo): ViewModel() {// el viewmodel es el tipo?

    private val tragosData = MutableLiveData<String>()

    fun setTrago(tragoName:String){
        tragosData.value =tragoName
    }

    init {
        setTrago("margarita")
    }

    val fetchTragosList = tragosData.distinctUntilChanged().switchMap {nombreTrago ->
        liveData(Dispatchers.IO) {//liveData ??
            emit(Resource.Loading())
            try {
                emit(repo.getTragosList(nombreTrago))

            }catch (e: Exception) {
                emit(Resource.Failure(e))
            }
        }
    }

    fun guardarTrago(trago:DrinkEntity){
        viewModelScope.launch {
            repo.insertTrago(trago)
        }
    }

    fun getTragosFavoritos() = liveData(Dispatchers.IO) {
        emit(Resource.Loading())
        try {
            emit(repo.getTragosFavoritos())

        }catch (e: Exception) {
            emit(Resource.Failure(e))
        }
    }

    fun deleteDrink(drink: Drink){
        viewModelScope.launch {
            repo.deleteDrink(drink)
        }
    }
}