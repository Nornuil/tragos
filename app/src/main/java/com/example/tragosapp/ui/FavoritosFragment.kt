package com.example.tragosapp.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tragosapp.AppDatabase
import com.example.tragosapp.R
import com.example.tragosapp.data.DataSourceImpl
import com.example.tragosapp.domain.RepoImpl
import com.example.tragosapp.ui.viewModel.MainViewModel
import com.example.tragosapp.ui.viewModel.VMFactory
import com.example.tragosapp.ValueObject.Resource
import com.example.tragosapp.data.model.Drink
import kotlinx.android.synthetic.main.fragment_favoritos.*

class FavoritosFragment : Fragment(), MainAdapter.OnTragoClickListener{

    private val viewModel by activityViewModels<MainViewModel> { VMFactory(
        RepoImpl(
            DataSourceImpl(AppDatabase.getDatabase(requireActivity().applicationContext))
        )
    )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favoritos, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupObservers()
    }

    private fun setupObservers(){
        viewModel.getTragosFavoritos().observe(viewLifecycleOwner, Observer {result ->
            when(result){
                is Resource.Loading -> {}
                is Resource.Success -> {
                    //Log.d("LISTA_FAVORITOS","${result.data}")
                    val lista = result.data.map {
                        Drink(it.tragoId,it.imagen,it.nombre,it.descripcion,it.hasAlcohol)
                    }
                    rvTragosFavoritos.adapter = MainAdapter(requireContext(),lista,this )
                }
                is Resource.Failure -> {}
            }
        })
    }

    private fun setupRecyclerView(){
        rvTragosFavoritos.layoutManager = LinearLayoutManager(requireContext())
        rvTragosFavoritos.addItemDecoration(DividerItemDecoration(requireContext(),DividerItemDecoration.VERTICAL))
    }

    override fun onTragoClick(drink: Drink,position: Int) {
        viewModel.deleteDrink(drink)
        rvTragosFavoritos.adapter?.notifyItemRemoved(position)
        rvTragosFavoritos.adapter?.notifyItemRangeRemoved(position, rvTragosFavoritos.adapter?.itemCount!!)
    }
}