package com.example.tragosapp.data

import com.example.tragosapp.AppDatabase
import com.example.tragosapp.ValueObject.Resource
import com.example.tragosapp.ValueObject.RetrofitClient
import com.example.tragosapp.data.model.Drink
import com.example.tragosapp.data.model.DrinkEntity
import com.example.tragosapp.domain.DataSource


// Simular el server
class DataSourceImpl(private val appDatabase: AppDatabase): DataSource {

    override suspend fun getTragoByName(tragoName:String):Resource<List<Drink>>{
        return Resource.Success(RetrofitClient.webService.getTragoByName(tragoName).drinkList)
    }

    override suspend fun insertTragoIntoRoom(trago:DrinkEntity){
        appDatabase.tragoDao().insertFavorite(trago)
    }

    override suspend fun getTragosFavoritos(): Resource<List<DrinkEntity>> {
        return Resource.Success(appDatabase.tragoDao().getAllFavoriteDrinks())
    }

    override suspend fun deleteDrink(drink: Drink) {
    appDatabase.tragoDao().deleteDrink(drink)
    }
}

