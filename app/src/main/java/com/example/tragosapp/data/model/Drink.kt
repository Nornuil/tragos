package com.example.tragosapp.data.model

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Drink(
    @SerializedName("idDrink")
    val tragoId: String = "",
    @SerializedName("strDrinkThumb")
    val imagen: String = "",
    @SerializedName("strDrink")
    val nombre: String = "",
    @SerializedName("strInstructions")
    val descripcion: String = "",
    @SerializedName("strAlcoholic")
    val hasAlcohol: String = "Non_Alcoholic"
): Parcelable //investigar

data class DrinkList(
    @SerializedName("drinks")
    val drinkList: List<Drink> = listOf())


@Entity(tableName = "tragosEntity")
data class DrinkEntity(
    @PrimaryKey
    val tragoId: String,
    @ColumnInfo(name= "tragoImagen")
    val imagen: String = "",
    @ColumnInfo(name= "tragoNombre")
    val nombre: String = "",
    @ColumnInfo(name= "tragoDescripcion")
    val descripcion: String = "",
    @ColumnInfo(name= "tragoHasAlcohol")
    val hasAlcohol: String = "Non_Alcoholic"
)