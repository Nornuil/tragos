package com.example.tragosapp.domain

import com.example.tragosapp.ValueObject.Resource
import com.example.tragosapp.ValueObject.RetrofitClient
import com.example.tragosapp.data.model.Drink
import com.example.tragosapp.data.model.DrinkEntity

interface DataSource {
    suspend fun getTragoByName(tragoName: String): Resource<List<Drink>>

    suspend fun insertTragoIntoRoom(trago: DrinkEntity)

    suspend fun getTragosFavoritos(): Resource<List<DrinkEntity>>

    suspend fun deleteDrink(drink: Drink)
}