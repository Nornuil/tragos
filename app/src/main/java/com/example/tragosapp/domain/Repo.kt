package com.example.tragosapp.domain

import com.example.tragosapp.ValueObject.Resource
import com.example.tragosapp.data.model.Drink
import com.example.tragosapp.data.model.DrinkEntity

interface Repo {
    suspend fun getTragosList(tragoName:String) : Resource<List<Drink>> //charlar esta linea
    suspend fun getTragosFavoritos(): Resource<List<DrinkEntity>>
    suspend fun insertTrago(trago:DrinkEntity)
    suspend fun deleteDrink(drink: Drink)
}